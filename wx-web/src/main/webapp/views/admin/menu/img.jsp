<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%--<%@include file="/comm/mytags.jsp" %>--%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>后台管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <%--<link rel="shortcut icon" href="${ctx}/static/img/favicon.ico">--%>

    <% String path = request.getContextPath(); %>

    <link rel="stylesheet" href="<%=path%>/static/plugins/layui/css/layui.css" media="all" />

    <script type="text/javascript" src="<%=path%>/static/plugins/layui/layui.js"></script>
    <style type="text/css">
        a {
            cursor: pointer;
        }

    </style>
</head>
<body class="childrenBody" style="font-size:12px;">
<fieldset class="layui-elem-field ">
    <legend style="font-size: 12px;color:#FF5722;">双击选择图标</legend>
    <div class="layui-field-box">
        <form class="layui-form layui-form-pane">
        <div class="layui-form-item" style="margin-bottom:0px;">
            <div class="layui-inline" style="margin-bottom:0px;">
                <div class="layui-form-mid layui-word-aux">
                    <%--<a class="select_img" data-id="&#xe652;" title="播放"><i class="layui-icon">&#xe652;</i></a>--%>
                    <li>
                        <a class="select_img" data-id="&#xe652;" title="播放">
                            <i class="layui-icon">&#xe652;</i>
                            <div class="name">播放</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux" >
                    <li>
                        <a class="select_img" data-id="&#xe651;" title="暂停">
                            <i class="layui-icon">&#xe651;</i>
                            <div class="name">暂停</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe6fc;" title="音乐">
                            <i class="layui-icon">&#xe6fc;</i>
                            <div class="name">音乐</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe688;" title="语音">
                            <i class="layui-icon">&#xe688;</i>
                            <div class="name">语音</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe645;" title="喇叭">
                            <i class="layui-icon">&#xe645;</i>
                            <div class="name">喇叭</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe611;" title="对话">
                            <i class="layui-icon">&#xe611;</i>
                            <div class="name">对话</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe614;" title="设置">
                            <i class="layui-icon">&#xe614;</i>
                            <div class="name">设置</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe60f;" title="隐身">
                            <i class="layui-icon">&#xe60f;</i>
                            <div class="name">隐身</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe615;" title="搜索">
                            <i class="layui-icon">&#xe615;</i>
                            <div class="name">搜索</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe641;" title="分享">
                            <i class="layui-icon">&#xe641;</i>
                            <div class="name">分享</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#x1002;" title="刷新">
                            <i class="layui-icon">&#x1002;</i>
                            <div class="name">刷新</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe63d;" title="加载">
                            <i class="layui-icon">&#xe63d;</i>
                            <div class="name">加载</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe620;" title="设置">
                            <i class="layui-icon">&#xe620;</i>
                            <div class="name">设置</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe628;" title="引擎">
                            <i class="layui-icon">&#xe628;</i>
                            <div class="name">引擎</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#x1006;" title="错号">
                            <i class="layui-icon">&#x1006;</i>
                            <div class="name">错号</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#x1007;" title="错号">
                            <i class="layui-icon">&#x1007;</i>
                            <div class="name">错号</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe629;" title="报表">
                            <i class="layui-icon">&#xe629;</i>
                            <div class="name">报表</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe600;" title="星星">
                            <i class="layui-icon">&#xe600;</i>
                            <div class="name">星星</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe617;" title="圆点">
                            <i class="layui-icon">&#xe617;</i>
                            <div class="name">圆点</div>
                        </a>
                    </li>
                </div>



                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe606;" title="客服">
                            <i class="layui-icon">&#xe606;</i>
                            <div class="name">客服</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe609;" title="发布">
                            <i class="layui-icon">&#xe609;</i>
                            <div class="name">发布</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe60a;" title="列表">
                            <i class="layui-icon">&#xe60a;</i>
                            <div class="name">列表</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe62c;" title="图表">
                            <i class="layui-icon">&#xe62c;</i>
                            <div class="name">图表</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#x1005;" title="正确">
                            <i class="layui-icon">&#x1005;</i>
                            <div class="name">正确</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe61b;" title="换肤">
                            <i class="layui-icon">&#xe61b;</i>
                            <div class="name">换肤</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe610;" title="对勾">
                            <i class="layui-icon">&#xe610;</i>
                            <div class="name">对勾</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe602;" title="向右">
                            <i class="layui-icon">&#xe602;</i>
                            <div class="name">向右</div>
                        </a>
                    </li>
                </div>


                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe603;" title="向左">
                            <i class="layui-icon">&#xe603;</i>
                            <div class="name">向左</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe62d;" title="表格">
                            <i class="layui-icon">&#xe62d;</i>
                            <div class="name">表格</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe62e;" title="树">
                            <i class="layui-icon">&#xe62e;</i>
                            <div class="name">树</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe62f;" title="上传">
                            <i class="layui-icon">&#xe62f;</i>
                            <div class="name">上传</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe61f;" title="添加">
                            <i class="layui-icon">&#xe61f;</i>
                            <div class="name">添加</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe601;" title="下载">
                            <i class="layui-icon">&#xe601;</i>
                            <div class="name">下载</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe631;" title="工具">
                            <i class="layui-icon">&#xe631;</i>
                            <div class="name">工具</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe654;" title="添加">
                            <i class="layui-icon">&#xe654;</i>
                            <div class="name">添加</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe642;" title="编辑">
                            <i class="layui-icon">&#xe642;</i>
                            <div class="name">编辑</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe640;" title="删除">
                            <i class="layui-icon">&#xe640;</i>
                            <div class="name">删除</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe61a;" title="向下">
                            <i class="layui-icon">&#xe61a;</i>
                            <div class="name">向下</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe621;" title="文件">
                            <i class="layui-icon">&#xe621;</i>
                            <div class="name">文件</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe632;" title="布局">
                            <i class="layui-icon">&#xe632;</i>
                            <div class="name">布局</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe618;" title="对勾">
                            <i class="layui-icon">&#xe618;</i>
                            <div class="name">对勾</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe633;" title="翻页">
                            <i class="layui-icon">&#xe633;</i>
                            <div class="name">翻页</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe61c;" title="404">
                            <i class="layui-icon">&#xe61c;</i>
                            <div class="name">404</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe634;" title="轮播">
                            <i class="layui-icon">&#xe634;</i>
                            <div class="name">轮播</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe607;" title="问号">
                            <i class="layui-icon">&#xe607;</i>
                            <div class="name">问号</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe635;" title="代码">
                            <i class="layui-icon">&#xe635;</i>
                            <div class="name">代码</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe636;" title="水滴">
                            <i class="layui-icon">&#xe636;</i>
                            <div class="name">水滴</div>
                        </a>
                    </li>
                </div>


                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe60b;" title="关于">
                            <i class="layui-icon">&#xe60b;</i>
                            <div class="name">关于</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe619;" title="向上">
                            <i class="layui-icon">&#xe619;</i>
                            <div class="name">向上</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe637;" title="日期">
                            <i class="layui-icon">&#xe637;</i>
                            <div class="name">日期</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe61d;" title="文件">
                            <i class="layui-icon">&#xe61d;</i>
                            <div class="name">文件</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe604;" title="top">
                            <i class="layui-icon">&#xe604;</i>
                            <div class="name">top</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe612;" title="好友">
                            <i class="layui-icon">&#xe612;</i>
                            <div class="name">好友</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe605;" title="对勾">
                            <i class="layui-icon">&#xe605;</i>
                            <div class="name">对勾</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe638;" title="窗口">
                            <i class="layui-icon">&#xe638;</i>
                            <div class="name">窗口</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe60c;" title="表情">
                            <i class="layui-icon">&#xe60c;</i>
                            <div class="name">表情</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe616;" title="正确">
                            <i class="layui-icon">&#xe616;</i>
                            <div class="name">正确</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe613;" title="好友">
                            <i class="layui-icon">&#xe613;</i>
                            <div class="name">好友</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe61e;" title="文件下载">
                            <i class="layui-icon">&#xe61e;</i>
                            <div class="name">文件下载</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe60d;" title="图片">
                            <i class="layui-icon">&#xe60d;</i>
                            <div class="name">图片</div>
                        </a>
                    </li>
                </div>

                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe64c;" title="链接">
                            <i class="layui-icon">&#xe64c;</i>
                            <div class="name">链接</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe60e;" title="记录">
                            <i class="layui-icon">&#xe60e;</i>
                            <div class="name">记录</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe622;" title="文件夹">
                            <i class="layui-icon">&#xe622;</i>
                            <div class="name">文件夹</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe64d;" title="unlink">
                            <i class="layui-icon">&#xe64d;</i>
                            <div class="name">unlink</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe639;" title="编辑">
                            <i class="layui-icon">&#xe639;</i>
                            <div class="name">编辑</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe623;" title="三角">
                            <i class="layui-icon">&#xe623;</i>
                            <div class="name">三角</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe63f;" title="单选框">
                            <i class="layui-icon">&#xe63f;</i>
                            <div class="name">单选框</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe643;" title="选中">
                            <i class="layui-icon">&#xe643;</i>
                            <div class="name">选中</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe647;" title="居中">
                            <i class="layui-icon">&#xe647;</i>
                            <div class="name">居中</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe648;" title="右对齐">
                            <i class="layui-icon">&#xe648;</i>
                            <div class="name">右对齐</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe649;" title="左对齐">
                            <i class="layui-icon">&#xe649;</i>
                            <div class="name">左对齐</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe626;" title="勾选框">
                            <i class="layui-icon">&#xe626;</i>
                            <div class="name">勾选框</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe627;" title="勾选框">
                            <i class="layui-icon">&#xe627;</i>
                            <div class="name">勾选框</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe62b;" title="加粗">
                            <i class="layui-icon">&#xe62b;</i>
                            <div class="name">加粗</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe63a;" title="沟通">
                            <i class="layui-icon">&#xe63a;</i>
                            <div class="name">沟通</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe624;" title="文件夹">
                            <i class="layui-icon">&#xe624;</i>
                            <div class="name">文件夹</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe63b;" title="手机">
                            <i class="layui-icon">&#xe63b;</i>
                            <div class="name">手机</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe650;" title="表情">
                            <i class="layui-icon">&#xe650;</i>
                            <div class="name">表情</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe63c;" title="表单">
                            <i class="layui-icon">&#xe63c;</i>
                            <div class="name">表单</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe62a;" title="tab">
                            <i class="layui-icon">&#xe62a;</i>
                            <div class="name">tab</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe64e;" title="代码">
                            <i class="layui-icon">&#xe64e;</i>
                            <div class="name">代码</div>
                        </a>
                    </li>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <li>
                        <a class="select_img" data-id="&#xe64a;" title="图片">
                            <i class="layui-icon">&#xe64a;</i>
                            <div class="name">图片</div>
                        </a>
                    </li>
                </div>

            </div>
        </div>
    </form>
    </div>
</fieldset>
<script type="text/javascript">
    layui.use(['form','layer','jquery'],function(){
        var $ = layui.jquery,
                form = layui.form,
                layer = parent.layer === undefined ? layui.layer : parent.layer;

        //选择图标
        $("body").on("dblclick",".select_img",function(){
            //得到当前iframe层的索引
            var index = parent.layer.getFrameIndex(window.name);

            //得到父级iframe层的索引
            var indexTop = parent.layer.getFrameIndex(top.window.name);
            var body = layer.getChildFrame('body', indexTop);
            //赋值
            var resImage = $(this).attr("data-id");
            body.find("#menu_icon").val(resImage);
            body.find("#menu_icon1").html("<li class='layui-icon' style='font-size: 30px;'>"+resImage+"</li>")

           parent.layer.close(index); //执行关闭
        });




    });
    
    function ddd() {
        
    }

</script>
</body>
</html>