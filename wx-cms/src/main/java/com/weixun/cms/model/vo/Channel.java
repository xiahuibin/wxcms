package com.weixun.cms.model.vo;

import java.util.List;

public class Channel {
    private Integer id;
    private String name;
    private String channel_parent;
    private String channel_english;
    private String channel_catalog;
    private String channel_page_name;
    private String channel_page_template;
    private String channel_index_name;
    private String channel_index_template;
    private String channel_index_count;
    private String channel_list_name;
    private String channel_list_template;
    private String channel_list_count;

    private List<Channel> children;        //子菜单

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChannel_parent() {
        return channel_parent;
    }

    public void setChannel_parent(String channel_parent) {
        this.channel_parent = channel_parent;
    }

    public String getChannel_english() {
        return channel_english;
    }

    public void setChannel_english(String channel_english) {
        this.channel_english = channel_english;
    }

    public String getChannel_catalog() {
        return channel_catalog;
    }

    public void setChannel_catalog(String channel_catalog) {
        this.channel_catalog = channel_catalog;
    }

    public String getChannel_page_name() {
        return channel_page_name;
    }

    public void setChannel_page_name(String channel_page_name) {
        this.channel_page_name = channel_page_name;
    }

    public String getChannel_page_template() {
        return channel_page_template;
    }

    public void setChannel_page_template(String channel_page_template) {
        this.channel_page_template = channel_page_template;
    }

    public String getChannel_index_name() {
        return channel_index_name;
    }

    public void setChannel_index_name(String channel_index_name) {
        this.channel_index_name = channel_index_name;
    }

    public String getChannel_index_template() {
        return channel_index_template;
    }

    public void setChannel_index_template(String channel_index_template) {
        this.channel_index_template = channel_index_template;
    }

    public String getChannel_index_count() {
        return channel_index_count;
    }

    public void setChannel_index_count(String channel_index_count) {
        this.channel_index_count = channel_index_count;
    }

    public String getChannel_list_name() {
        return channel_list_name;
    }

    public void setChannel_list_name(String channel_list_name) {
        this.channel_list_name = channel_list_name;
    }

    public String getChannel_list_template() {
        return channel_list_template;
    }

    public void setChannel_list_template(String channel_list_template) {
        this.channel_list_template = channel_list_template;
    }

    public String getChannel_list_count() {
        return channel_list_count;
    }

    public void setChannel_list_count(String channel_list_count) {
        this.channel_list_count = channel_list_count;
    }

    public List<Channel> getChildren() {
        return children;
    }

    public void setChildren(List<Channel> children) {
        this.children = children;
    }
}
