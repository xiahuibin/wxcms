package com.weixun.utils.file;


import java.io.*;

public class FileUtils {
    public static boolean SaveFile(String filepath)
    {
        boolean flag = false;
        //加载模版
        Writer out = null;

        try {
            //设置要解析的模板所在的目录

            //设置输出文件
            File file = new File(filepath);
            //判断目录是否存在，不存在则删除并创建
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            //页面存在则删除
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8"));
            flag=true;
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }finally{
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
                flag = false;
            }
        }
        return flag;
    }

    /**
     * 获取文件后缀
     *
     * @param @param fileName
     * @param @return 设定文件
     * @return String 返回类型
     */
    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf('.'), fileName.length());
    }
}
